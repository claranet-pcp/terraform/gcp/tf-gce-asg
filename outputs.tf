output "instance_group_manager_id" {
  value = "${google_compute_instance_group_manager.manager.id}"
}

output "instance_group" {
  value = ["${google_compute_instance_group_manager.manager.*.instance_group}"]
}

output "autoscaler_id" {
  value = "${google_compute_autoscaler.scaler.id}"
}
