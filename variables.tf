variable "name"              {}
variable "instance_template" {}

variable "enabled" {
  default = "1"
}

variable "gce_region" {
  default = "europe-west1"
}

variable "gce_zones" {
  default = {
    europe-west1 = "europe-west1-b,europe-west1-c,europe-west1-d"
  }
}

variable "asg_min" {
  default = "1"
}

variable "asg_max" {
  default = "1"
}
