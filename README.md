# tf-gce-asg

## Variables
* `name` -
* `instance_template` -
* `enabled` (1) -
* `gce_region` (europe-west1) -
* `gce_zones` (europe-west1 = "europe-west1-b,europe-west1-c,europe-west1-d") -
* `asg_min` (1) -
* `asg_max` (1) -

## Outputs
* `instance_group_manager_id` -
* `autoscaler_id` -

## Example usage

```
module "sso-asg" {
  source = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-gce-asg.git?ref=0.7.11-1"

  name              = "sso"
  instance_template = "${module.sso-instance-template.link}"
  enabled           = 3
  asg_min           = 1
  asg_max           = 1
}
```
