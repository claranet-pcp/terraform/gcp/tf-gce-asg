resource "google_compute_instance_group_manager" "manager" {
  count              = "${var.enabled}"
  name               = "${var.name}-instance-group-${count.index}-${var.gce_region}"
  base_instance_name = "${var.name}"
  instance_template  = "${var.instance_template}"
  update_strategy    = "RESTART"
  zone               = "${element(split(",", lookup(var.gce_zones, var.gce_region)), count.index)}"
}

resource "google_compute_autoscaler" "scaler" {
  count  = "${var.enabled}"
  name   = "${var.name}-autoscaler-${count.index}"
  zone   = "${element(split(",", lookup(var.gce_zones, var.gce_region)), count.index)}"
  target = "${element(google_compute_instance_group_manager.manager.*.self_link, count.index)}"

  autoscaling_policy = {
    max_replicas    = "${var.asg_max}"
    min_replicas    = "${var.asg_min}"
    cooldown_period = 60

    cpu_utilization {
      target = 0.6
    }
  }
}
